## Purpose

The script will apply basic level protection against Petya malware by:

1. Applying a so-called vaccination which stops the encryption occurring.
2. Disabling SMBv1 - If you have technology which uses this protocol, they may stop working.
3. Blocking ports 445 and 139 in Windows Firewall. This might cause issues with AD, File and Printing and Group Policy.

## Possible Additions if network is already under attack

1. Disable WMIC.
2. Disable PSEXEC.
