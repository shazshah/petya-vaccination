@echo off
REM Author: 	Shaz
REM Purpose: 	This script will not prevent Petya Malware from executing but it will stop
REM        		the virus from encrypting the affected hard drive (the vaccination)
REM 			and stop it from propogating (in or out).
REM 			It does this by creating some files (perfc), disabling SMBv1 which has
REM 			a known exploit and blocking two ports used by the malware (445, 139) 
REM 			to spread the malware and/or gain entry on to a target machine. Also, 
REM				good practice to perform Windows Update.
REM	Version:	Version 1
REM Sources: 	BleepingComputer and Microsoft TechNet

echo Administrative permission required. Detecting permissions...
echo.
net session >nul 2>&1

if %errorlevel% == 0 (
	echo Disabling SMBv1. This will prevent the virus exploiting weakness in the SMBv1 protocol and propogating.
	echo.
	sc.exe config lanmanworkstation depend=bowser/mrxsmb20/nsi
	sc.exe config mrxsmb10 start= disabled
	
	echo Blocking SMB on Local Firewall to stop further propogation
	echo.
	netsh advfirewall firewall add rule name="Block TCP SMB to prevent Petya malware propogation 0" protocol=TCP dir=in remoteport=445,139 action=block
	netsh advfirewall firewall add rule name="Block UDP SMB to prevent Petya malware propogation 0" protocol=UDP dir=in remoteport=445,139 action=block
	netsh advfirewall firewall add rule name="Block TCP SMB to prevent Petya malware propogation 0" protocol=TCP dir=out remoteport=445,139 action=block
	netsh advfirewall firewall add rule name="Block UDP SMB to prevent Petya malware propogation 0" protocol=UDP dir=out remoteport=445,139 action=block
	
	echo Applying Perfc Vaccination. This will prevent the virus from encrypting the drive
	echo Reboot computer when script ends
	
	if exist C:\Windows\perfc (
		echo Computer alreday vaccinated for NotPetya/Petya/Petna/SortaPetya.
		echo.
	) else (
		echo This is a NotPetya/Petya/Petna/SortaPetya Vaccination file. Do not remove. > C:\Windows\perfc
		echo This is a NotPetya/Petya/Petna/SortaPetya Vaccination file. Do not remove. > C:\Windows\perfc.dll
		echo This is a NotPetya/Petya/Petna/SortaPetya Vaccination file. Do not remove. > C:\Windows\perfc.dat
		
		attrib +R C:\Windows\perfc
		attrib +R C:\Windows\perfc.dll
		attrib +R C:\Windows\perfc.dat
		
		echo Computer Vaccinated with perfc for current version of NotPetya/Petya/Petna/SortaPetya.
	)
	
) else (
	echo failure: you must run this batch file as Admin.
)
pause